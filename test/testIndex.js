"use strict";
const assert = require('chai').assert;
const expect = require('chai').expect;
const sinon = require("sinon");
const index = require("../src/index");


let sandbox = null;
describe("Test Index", function() {
    this.timeout(10000);

    before(async function() {
    });
    after(async function() {
    });

    beforeEach(function() {
        sandbox = sinon.createSandbox();
    });
    
    afterEach(function() {
        sandbox.restore();
    });

    it("Test hello Normal Execution", function(done) {
        index.execute();
        done();
    });

});
